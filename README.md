# Gigachads binary alarmclock
I am tired of relying on my phone to set alarms. \
Therefore I am making the ridiculus move of a 100% hardware clock. \
To work with this project you need:
* [logisim evolution](https://github.com/logisim-evolution/logisim-evolution) FOSS logic simulator
* [kicad](https://www.kicad.org/) FOSS PCB software (used at CERN)

The only logisim file is `concept.circ`, the rest i kicad stuff.
